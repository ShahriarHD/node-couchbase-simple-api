const express = require ('express');
const app = express();
const db = require('./schema/db');
const todoRoutes = require('./routes/todoRoutes');
const path = require('path');

// const cors = require('cors');//development env.

// app.use(cors());//development env.

todoRoutes(app);

app.use(express.static(path.join(__dirname, 'build')));
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname,'build','index.html'));
});

db.ensureIndices((dberror) => {
    if (dberror) {
        console.error("error connecting to database");
        throw dberror;
    }
    port = process.env.port || 8000;
    const server = app.listen(port, () => {
        console.log("app is listening on port" + port);
    });

    server.on("uncaughtException", (error) => {
        console.error(error);
    });

    server.on("error", (error) => {
        console.error(error);
    });
});