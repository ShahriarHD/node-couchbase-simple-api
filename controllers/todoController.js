const todoModel = require ('../schema/model/todoModel.js');

exports.create = (req, res) => {
    const text = req.body.text;
    
    if (!text) {
        return res.sendStatus(400);
    }

    todoModel.createAndSave(text,(error, result) => {
        if (error) {
            console.error(error)
            return res.sendStatus(500);
        }
        return res.json(result);
    });
}

exports.list = (req, res) => {
    todoModel.getAll((error, result) => {
        if (error) {
            console.error(error);
            return res.sendStatus(500);
        }
        return res.json(result);
    });
}

exports.update = (req, res) => {
    const todoID = req.params.todo_id;
    const text = req.body.text;
    todoModel.findByIdAndUpdate(text, todoID, (error) => {
        if (error) {
            console.error(error);
            return res.sendStatus(500);
        }
        return res.sendStatus(200);
    });
}

exports.remove = (req, res) => {
    const todoID = req.params.todo_id;
    todoModel.findByIdAndRemove(todoID, (error, result) => {
        if (error) {
            console.error(error);
            return res.sendStatus(500);
        }
        return res.sendStatus(200);
    });
}