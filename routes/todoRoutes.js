const todoController = require('../controllers/todoController');
const jsonParser = require('body-parser').json({type: '*/json'});

module.exports = function (app) {
    app.route('/todos')
        .post(jsonParser, todoController.create)
        .get(todoController.list);
    app.route('/todos/:todo_id')
        .put(jsonParser, todoController.update)
        .delete(todoController.remove);
}