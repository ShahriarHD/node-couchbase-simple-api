const db = require('../db');

const todoModel = db.model('todo', {
    todoID: {type:'string', auto: 'uuid', readonly: true}, 
    text: {type: 'string'},
    createdOn: {type: 'Date', default: new Date()},
},{
    index: {
        findByID: {
            by: 'todoID',
            type: 'refdoc',
        }
    }
});

todoModel.createAndSave = function (text, callback) {
    this.create({ text }, callback);
}

todoModel.getAll = function(callback) {
    this.find({}, {sort: 'createdOn'}, callback);
}

todoModel.findByIdAndUpdate = function (text, id, callback) {
    this.getById(id, function(error, todoInstance){
        if (error){
            return callback(error);
        }
        todoInstance.text = text;
        todoInstance.save(callback);
    });
}

todoModel.findByIdAndRemove = function (id, callback) {
    this.getById(id, function (error, todoInstance) {
        if (error) {
            return callback(error);
        }
        todoInstance.remove(callback);
    });
}

module.exports = todoModel;