const couchbase = require('couchbase');
const ottoman = require('ottoman');

const cluster = new couchbase.Cluster('http://localhost:8091'); // TODO read from config store
const userName = process.env.couchbaseAdminUserName || 'Administrator';
const password = process.env.couchbaseAdminPassword || '123456'
cluster.authenticate(userName, password);
const bucket = cluster.openBucket('todoapp');

ottoman.bucket = bucket;

module.exports = ottoman;